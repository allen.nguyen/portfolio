
.PHONY: check-env
check-env:
ifndef ENVIRONMENT
	$(error ENVIRONMENT is undefined)
endif
ifndef AWS_ACCESS_KEY_ID
	$(error AWS_ACCESS_KEY_ID is undefined)
endif
ifndef AWS_SECRET_ACCESS_KEY
	$(error AWS_SECRET_ACCESS_KEY is undefined)
endif


.PHONY: init
init: check-env
	terraform init
	-terraform workspace new $(ENVIRONMENT)
	terraform workspace select $(ENVIRONMENT)

.PHONY: deploy
deploy: check-env build init
	terraform apply --auto-approve

.PHONY: destroy
destroy: check-env init
	terraform destroy --auto-approve

.PHONY: build
build: install
	npm run build --prefix ./frontend

.PHONY: start
start:
	npm run serve --prefix ./frontend

.PHONY: lint
lint:
	npm run lint --prefix ./frontend

.PHONY: install
install:
	npm install --prefix ./frontend
