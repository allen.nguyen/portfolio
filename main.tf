
provider "aws" {
  version = "~> 2.0"
  region  = "us-west-2"
}

terraform {
  backend "s3" {
    bucket = "portfolio-terraform-backend"
    key = "tfstate"
    region  = "us-west-2"
  }
}

resource "aws_s3_bucket" "portfolio_s3_bucket" {
  bucket = "portfolio-web-app-${terraform.workspace}"
  acl    = "public-read"
  force_destroy = true
  policy = <<POLICY
{
  "Version":"2012-10-17",
  "Statement":[
    {
      "Sid":"AddPerm",
      "Effect":"Allow",
      "Principal": "*",
      "Action":["s3:GetObject"],
      "Resource":["arn:aws:s3:::portfolio-web-app-${terraform.workspace}/*"]
    }
  ]
}
POLICY
  website {
    index_document = "index.html"
  }
}

resource "null_resource" "remove_and_upload_to_s3" {
  triggers = {
    always_run = "${timestamp()}"
  }
  provisioner "local-exec" {
    command = "aws s3 sync ${path.module}/frontend/dist s3://${aws_s3_bucket.portfolio_s3_bucket.id}"
  }
}

output "s3_endpoint" {
  value = aws_s3_bucket.portfolio_s3_bucket.website_endpoint
}

output "s3_bucket_name" {
  value = aws_s3_bucket.portfolio_s3_bucket.bucket
}





