# Allen Nguyen's Portfolio Website

## Introduction
This project is meant to demonstrate my experience and try out VUE.js. It should show off my experience with a front end JS framework (Vue), cloud infrastructure (Terraform), and pipelines (GitLab CI)

My experience is not necessarily limited to the technologies shown in this project. Many of the technologies I chose were due to popularity (Vue, Terraform, Docker, Python), however, I did choose Vue because I wanted to try something new.

## Running locally
To run a local instance run `make start` from the root of this project.

## Deploy

### Deploying to production
This application is automatically deployed to production upon merging to master.

### Deploying your own instance
The following section will allow you to deploy my application to your AWS instance. In my opinion it is important that new developers can look at a README and easily deploy their own instance of an application for testing/demoing/discovery.

#### Before you deploy
Ensure you have the following tools installed:
- Terraform
    - Terraform install instructions can be found here: https://learn.hashicorp.com/terraform/azure/install_az
    - Brew users (MacOS) can install with `brew install terraform`
- IAM user with programmatic access
    - You will need the IAM users Access Key ID and Secret access key
    - The IAM user will require read and write access to the following resources:
        - S3
- Make
    - If your on a Unix-like (MacOS, Ubuntu, etc.) then you probably already have Make installed
    - Windows install instructions can be found here: http://gnuwin32.sourceforge.net/packages/make.htm

Once you have the requisite tools installed, set the following environment variables:
- ENVIRONMENT
    - This variable sets the postfix of your AWS resources.
- AWS_ACCESS_KEY_ID
- AWS_SECRET_ACCESS_KEY
#### Deploy
To deploy your own instance run `make deploy` from the root of this project.

After your website has been deployed terraform will output an s3_endpoint which you can use to view your site.








